function [v,r] = version
% VERSION Tag and name of this release.
v = '0.1';
if nargout>1
    r = 'Avengers (alpha)';
end
end
